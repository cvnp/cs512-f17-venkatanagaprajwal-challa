import numpy as np
import math
from numpy.linalg import inv

A = np.array([1,2,3])
B = np.array([4,5,6])
C = np.array([-1, 1, 3])

MAGNITUDE_A = np.linalg.norm(A)

print A/MAGNITUDE_A