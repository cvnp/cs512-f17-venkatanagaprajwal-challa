import numpy as np
import math
from numpy.linalg import inv

A = np.matrix([[1, 2], [3, 2]])
B = np.matrix([[2, -2],[-2, 5]])

j, V = np.linalg.eig(B)

e1 = [-0.89442719,-0.4472136 ]
e2 = [0.4472136, -0.89442719 ]

print np.dot(e1,e2)