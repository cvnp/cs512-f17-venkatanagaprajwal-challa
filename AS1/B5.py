import numpy as np
import math
from numpy.linalg import inv

A = np.matrix([[1, 2, 3], [4, -2, 3], [0, 5, -1]])
B = np.matrix([[1, 2, 1], [2, 1, -4], [3, -2, 1]])
C = np.matrix([[1, 2, 3], [4, 5, 6], [-1, -1, 3]])

val1 = B.item(0,0)*B.item(1,0) + B.item(0,1)*B.item(1,1) + B.item(0,2)*B.item(1,2)
val2 = B.item(1,0)*B.item(2,0) + B.item(1,1)*B.item(2,1) + B.item(1,2)*B.item(2,2)
val3 = B.item(0,0)*B.item(2,0) + B.item(0,1)*B.item(2,1) + B.item(0,2)*B.item(2,2)

print val1
print val2
print val3

print "As all values are zero B is orthogonal set"