import numpy as np
import math
from numpy.linalg import inv

A = np.matrix([[1, 2, 3], [4, -2, 3], [0, 5, -1]])
B = np.matrix([[1, 2, 1], [2, 1, -4], [3, -2, 1]])
C = np.matrix([[1, 2, 3], [4, 5, 6], [-1, -1, 3]])


DET_A = np.linalg.det(A)

print DET_A

DET_C = np.linalg.det(C)

print DET_C