import numpy as np
import math
from numpy.linalg import inv

A = np.array([1,2,3])
B = np.array([4,5,6])
C = np.array([-1, 1, 3])

print "let G = Xi + Yj + Zk; "
print "A.G = X+2Y+3Z"
print "As they are perpendicular dot  product is zero"
print "solution of X+2Y+3Z = 0 is X = 1 , Y = 1 , Z = -1"