import numpy as np
import math
from numpy.linalg import inv

A = np.array([1,2,3])
B = np.array([4,5,6])
C = np.array([-1, 1, 3])

A_UNIT = np.linalg.norm(A)
B_UNIT = np.linalg.norm(B)

val = np.dot(A,B)

val2 = A_UNIT*B_UNIT

val3 = np.arccos(val/val2)

print val3