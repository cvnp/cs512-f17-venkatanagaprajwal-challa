import numpy as np
import math
from numpy.linalg import inv

A = np.array([1,2,3])
B = np.array([4,5,6])
C = np.array([-1, 1, 3])

print "vector perpendicular to A and B is AXB"

GG = np.cross(A,B)

print GG