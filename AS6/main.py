import cv2
import numpy as np
from matplotlib import pyplot as plt
import os
import sys



Final_Kernel = np.array([[1/12, 1/6, 1/12],[1/6, 0, 1/6],[1/12, 1/6, 1/12]],np.float32)

def dxdy(img1, img2):

	Kernel1 = 0.25 * np.array(([-1, 1], [-1, 1]))
	Kernel2 = 0.25 * np.array(([-1, -1], [1, 1]))
	
	dx = cv2.filter2D(img1, -1, Kernel1) + cv2.filter2D(img2, -1, Kernel1)
	dy = cv2.filter2D(img1, -1, Kernel2) + cv2.filter2D(img2, -1, Kernel2)
	dt = cv2.filter2D(img1, -1, 0.25 * np.ones((2, 2))) + cv2.filter2D(img2, -1, -0.25 * np.ones((2, 2)))

	return dx,dy,dt

	
def HORNSHUNCKFLOW(img1,img2,const):
	img1 = img1.astype(np.float32)
	img2 = img2.astype(np.float32)
	
	[Idx, Idy, Idt] = dxdy(img1, img2)
	
	u = np.zeros_like(img1)
	v = np.zeros_like(img1)
	
	for interation in xrange(100):
		newu = np.copy(u)
		newv = np.copy(v)
		
		convolved_u = cv2.filter2D(newu.astype(np.float32), -1, Final_Kernel)
		convolved_v = cv2.filter2D(newv.astype(np.float32), -1, Final_Kernel)
		
		u = convolved_u - Idx * ((Idx*convolved_u + Idy*convolved_v + Idt) / (const**2 + Idx**2 + Idy**2))
		v = convolved_v - Idy * ((Idx*convolved_u + Idy*convolved_v + Idt) / (const**2 + Idx**2 + Idy**2))
		
	return u, v

if len(sys.argv) == 1:
	print ("Welcome")
	print ("To get optical flow stay still at first and move later")
	print ("after you are done hit ESC")
	print (" ")
	cap = cv2.VideoCapture(0)
	img1 = None
	img2 = None
	img3 = None
	ret, old_frame = cap.read()
	img1 = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
	cv2.imshow("img1",img1)
	while (1):
		ret,frame = cap.read()
		img2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		img3 = img2.copy()
		cv2.imshow("img2",img2)
		key = cv2.waitKey(1)
		if key == 27:
			cap.release()
			break
		
	
	img1 = np.float32(img1)
	img2 = np.float32(img2)
	[u,v] = HORNSHUNCKFLOW(img1, img2, 0.5)
	a = np.arange(0, img1.shape[1], 1)
	b = np.arange(0, img1.shape[0], 1)
	plt.figure()
	plt.imshow(img3, cmap='gray', interpolation='bicubic')	
	step = 10
	plt.quiver(a[::step], b[::step], u[::step, ::step], v[::step, ::step],color='r', pivot='middle', headwidth=2, headlength=3)
	cap.release()
	plt.show()
	cv2.waitKey(0)
	cv2.destroyAllWindows()
	
	
		

		