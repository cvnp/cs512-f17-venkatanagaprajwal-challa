#prajwal's program
import cv2
import sys
import numpy as np

if len(sys.argv) == 2:
	filename = sys.argv[1]
	img = cv2.imread(filename)
	cv2.imshow('test',img)
	while(True):
		k = cv2.waitKey(0)
		if k == ord('i'):
			#cv2.imwrite('testout.png',img)
			img = cv2.imread('img1.jpg',0)
			#cv2.imshow('test',img)
			#cv2.destroyAllWindows()
		elif k == ord('g'):
			img = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			cv2.imshow('OPENCVGREYSCALE',img)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',img)
			#cv2.destroyAllWindows()
		elif k == ord('w'):
			cv2.imwrite('out.jpg',img)
			#cv2.destroyAllWindows()
		elif k == ord('G'):
			img = cv2.imread(filename)
			values = [0.114,0.587,0.299]
			newone = np.array(values).reshape((1,3))
			newimg = cv2.transform(img, newone)
			cv2.imshow('customgreyscale',newimg)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',newimg)
			#cv2.destroyAllWindows()
		elif k == ord('c'):
			cnt = 1
			img  = cv2.imread(filename)
			if cnt == 1:
				blue = img.copy()
				blue[:,:,1] = 0
				blue[:,:,2] = 0
				cv2.imshow('only blue',blue)
				kk = cv2.waitKey(0)
				if kk == ord('w'):
					cv2.imwrite('out.jpg',blue)
				elif kk == ord('c'):
					red = img.copy()
					red[:,:,0] = 0
					red[:,:,1] = 0
					cv2.imshow('only red',red)
					kkk = cv2.waitKey(0)
					if kkk == ord('w'):
						cv2.imwrite('out.jpg',red)
					elif kkk == ord('c'):
						green = img.copy()
						green[:,:,0] = 0
						green[:,:,2] = 0
						cv2.imshow('only green',green)
						kkkk = cv2.waitKey(0)
						if kkkk == ord('w'):
							cv2.imwrite('out.jpg',green)

		elif k == ord('s'):
			def sliderhandler(n):
				img  = cv2.imread(filename)
				src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				kernel = np.ones((n,n),np.float32)/(n*n)
				dst = cv2.filter2D(src,-1,kernel)
				cv2.imshow(win,dst)
			
			win = 'trackbar smoothin'
			img  = cv2.imread(filename)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,img)
			cv2.createTrackbar('s',win,0,10,sliderhandler)
			
		elif k == ord('S'):
			def sliderhandler(n):
				#anchor = (-1,-1)
				delta = 0
				ddepth = -1
				img = cv2.imread(filename)
				h,w = img.shape[:2]
				dst = np.zeros((h, w))
				src = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
				kernel = np.ones((n,n),np.float32)/(n*n)
				dst = cv2.filter2D(src,-1,kernel,dst,(-1,-1),0,cv2.BORDER_DEFAULT)
				cv2.imshow(win,dst)
			
			win = 'trackbar smoothing self'
			img  = cv2.imread(filename)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,img)
			cv2.createTrackbar('s',win,0,10,sliderhandler)
		
		elif k == ord('d'):
			img  = cv2.imread(filename)
			height, width = img.shape[:2]
			ress = cv2.resize(img,(width/2,height/2),interpolation = cv2.INTER_AREA)
			cv2.imshow('downsampled.jpg',ress)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',ress)
			#cv2.destroyAllWindows()
		elif k == ord('D'):
			img  = cv2.imread(filename)
			h, w = img.shape[:2]
			img = cv2.pyrDown(img,dstsize = (w/2,h/2))
			cv2.imshow('downsampledGaussian.jpg',img)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',img)
			#cv2.destroyAllWindows()
		elif k == ord('x'):
			img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
			normalizedimg = cv2.normalize(sobelx, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imwrite('Normalizedsobelx.jpg',normalizedimg)
			newx = cv2.imread('Normalizedsobelx.jpg')
			cv2.imshow('Normalizedsobelx',newx)
			cv2.imshow('sobelx.jpg',sobelx)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			#cv2.destroyAllWindows()
		elif k == ord('y'):
			img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
			normalizedimg = cv2.normalize(sobely, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imshow('sobely.jpg',sobely)
			cv2.imwrite('Normalizedsobely.jpg',normalizedimg)
			newy = cv2.imread('Normalizedsobely.jpg')
			cv2.imshow('Normalizedsobely',newy)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			#cv2.destroyAllWindows()
		elif k == ord('m'):
			img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobelx = cv2.Sobel(img,cv2.CV_64F,1,0,ksize=5)
			sobely = cv2.Sobel(img,cv2.CV_64F,0,1,ksize=5)
			magnitudeofgradient = np.sqrt(sobelx**2 + sobely**2)
			final_sobel = np.uint8(255*magnitudeofgradient/np.max(magnitudeofgradient))
			normalizedimg = cv2.normalize(final_sobel, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imshow('Normalizedmagnitudesobel.jpg',normalizedimg)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			#cv2.destroyAllWindows()
		
		elif k == ord('r'):
			def sliderhandler(n):
				img  = cv2.imread(filename)
				src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				angle = n
				rows = src.shape[0]
				cols = src.shape[1]
				M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
				dst = cv2.warpAffine(src,M,(cols,rows),flags=cv2.WARP_INVERSE_MAP)
				cv2.imshow(win,dst)
			
			win = 'rotation'
			img = cv2.imread(filename)
			img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,img)
			cv2.createTrackbar('s',win,0,360,sliderhandler)

		elif k == ord('h'):
			print "welocme"
			print "the following functions are initiated when corresponding keys are pressed"
			print "a - relod the image"
			print "w - to download  image to out.jpg"
			print "g - to cnovert image to grayscale using opencv functions"
			print "G - to convert image to greyscale using custom function"
			print "c - cycle through R,B,G color channel of image"
			print "s - smoothing image using trackbar using opencv functions"
			print "S - smoothing image using trackbar using custom functions"
			print "d - downsample image by 2 without smoothing"
			print "D - downsample image by 2 with smoothing"
			print "x - convolve greyscale image with x derivative filter . to obtain normalized image press w"
			print "y - convolve greyscale image with y derivative filter . to obtain normalized image press w"
			print "m - magnitude of image gradient"
			print "r - rotate image using trackbar"
			print "ESC to quit"

		elif k == 27:
			cv2.destroyAllWindows()
			break


elif len(sys.argv) < 2:
	cap = cv2.VideoCapture(0)
	retval,img = cap.read()
	cv2.imshow('test',img)
	while(True):
		k = cv2.waitKey(0)
		if k == ord('i'):
			#cv2.imwrite('testout.png',img)
			#img = cv2.imread('img1.jpg',0)
			#cv2.imshow('test',img)
			#cv2.destroyAllWindows()
			retval,img = cap.read()
			cv2.imshow('test',img)
		elif k == ord('g'):
			src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow('OPENCVGREYSCALE',src)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',src)
			#cv2.destroyWindow('OPENCVGREYSCALE')
		elif k == ord('w'):
			cv2.imwrite('out.jpg',img)
			#cv2.destroyAllWindows()
		elif k == ord('G'):
			src = img.copy()
			values = [0.114,0.587,0.299]
			newone = np.array(values).reshape((1,3))
			newimg = cv2.transform(src, newone)
			cv2.imshow('customgreyscale',newimg)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',newimg)
			#cv2.destroyAllWindows()
		elif k == ord('c'):
			cnt = 1
			if cnt == 1:
				blue = img.copy()
				blue[:,:,1] = 0
				blue[:,:,2] = 0
				cv2.imshow('only blue',blue)
				kk = cv2.waitKey(0)
				if kk == ord('w'):
					cv2.imwrite('out.jpg',blue)
				elif kk == ord('c'):
					red = img.copy()
					red[:,:,0] = 0
					red[:,:,1] = 0
					cv2.imshow('only red',red)
					kkk = cv2.waitKey(0)
					if kkk == ord('w'):
						cv2.imwrite('out.jpg',red)
					elif kkk == ord('c'):
						green = img.copy()
						green[:,:,0] = 0
						green[:,:,2] = 0
						cv2.imshow('only green',green)
						kkkk = cv2.waitKey(0)
						if kkkk == ord('w'):
							cv2.imwrite('out.jpg',green)

		elif k == ord('s'):
			def sliderhandler(n):
				src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				kernel = np.ones((n,n),np.float32)/(n*n)
				dst = cv2.filter2D(src,-1,kernel)
				cv2.imshow(win,dst)
			
			win = 'trackbar smoothing'
			imgg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,imgg)
			cv2.createTrackbar('s',win,0,10,sliderhandler)

		elif k == ord('S'):
			def sliderhandler(n):
				#anchor = (-1,-1)
				delta = 0
				ddepth = -1
				h,w = img.shape[:2]
				dst = np.zeros((h,w))
				src = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
				kernel = np.ones((n,n),np.float32)/(n*n)
				dst = cv2.filter2D(src,-1,kernel,dst,(-1,-1),0,cv2.BORDER_DEFAULT)
				cv2.imshow(win,dst)
			
			win = 'trackbar smoothing self'
			imgg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,imgg)
			cv2.createTrackbar('s',win,0,10,sliderhandler)

		elif k == ord('d'):
			height, width = img.shape[:2]
			ress = cv2.resize(img,(width/2,height/2),interpolation = cv2.INTER_AREA)
			cv2.imshow('downsampled',ress)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',ress)
			

		elif k == ord('D'):
			h, w = img.shape[:2]
			img = cv2.pyrDown(img,dstsize = (w/2,h/2))
			cv2.imshow('downsampledGaussian.jpg',img)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',img)
			

		elif k == ord('x'):
			#img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobelx = cv2.Sobel(src,cv2.CV_64F,1,0,ksize=5)
			normalizedimg = cv2.normalize(sobelx, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imwrite('Normalizedsobelx.jpg',normalizedimg)
			newx = cv2.imread('Normalizedsobelx.jpg')
			cv2.imshow('sobelx',sobelx)
			newx = cv2.imread('Normalizedsobelx.jpg')
			cv2.imshow('Normalizedsobelx',newx)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			

		elif k == ord('y'):
			#img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobely = cv2.Sobel(src,cv2.CV_64F,0,1,ksize=5)
			normalizedimg = cv2.normalize(sobely, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imshow('sobely',sobely)
			cv2.imwrite('Normalizedsobely.jpg',normalizedimg)
			newy = cv2.imread('Normalizedsobely.jpg')
			cv2.imshow('Normalizedsobely',newy)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			

		elif k == ord('m'):
			#img  = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
			src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			h, w = img.shape[:2]
			normalizedimg = np.zeros((h, w))
			sobelx = cv2.Sobel(src,cv2.CV_64F,1,0,ksize=5)
			sobely = cv2.Sobel(src,cv2.CV_64F,0,1,ksize=5)
			magnitudeofgradient = np.sqrt(sobelx**2 + sobely**2)
			final_sobel = np.uint8(255*magnitudeofgradient/np.max(magnitudeofgradient))
			normalizedimg = cv2.normalize(final_sobel, normalizedimg,0,255,cv2.NORM_MINMAX)
			cv2.imshow('Normalizedmagnitudesobel',normalizedimg)
			oo = cv2.waitKey(0)
			if oo == ord('w'):
				cv2.imwrite('out.jpg',normalizedimg)
			

		elif k == ord('r'):
			def sliderhandler(n):
				src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
				angle = n
				rows = src.shape[0]
				cols = src.shape[1]
				M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
				dst = cv2.warpAffine(src,M,(cols,rows),flags=cv2.WARP_INVERSE_MAP)
				cv2.imshow(win,dst)
			
			win = 'rotation'
			src = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
			cv2.imshow(win,img)
			cv2.createTrackbar('s',win,0,360,sliderhandler)

		elif k == ord('h'):
			print "welocme"
			print "the following functions are initiated when corresponding keys are pressed"
			print "a - relod the image"
			print "w - o download  image to out.jpg"
			print "g - to cnovert image to grayscale using opencv functions"
			print "G - to convert image to greyscale using custom function"
			print "c - cycle through R,B,G color channel of image"
			print "s - smoothing image using trackbar using opencv functions"
			print "S - smoothing image using trackbar using custom functions"
			print "d - downsample image by 2 without smoothing"
			print "D - downsample image by 2 with smoothing"
			print "x - convolve greyscale image with x derivative filter press w to get normalized image"
			print "y - convolve greyscale image with y derivative filter press w to get normalized image"
			print "m - magnitude of image gradient"
			print "r - rotate image using trackbar"
			print "ESC - quit"

		elif k == 27:
			cv2.destroyAllWindows()
			cap.release()
			break
