import cv2
import numpy as np
import sys

def none(x):
	pass
		
def harris_corner_detection(img,neighbourhood_size,k,threshold_value):
		dy, dx = np.gradient(img)
		derivative_x = dx**2
		derivative_y = dy**2
		derivative_xy = dy*dx
		height, width = img.shape[:2]
		temp_img = img.copy()
		new_temp_img = cv2.cvtColor(temp_img, cv2.COLOR_GRAY2RGB)
		window_size = neighbourhood_size/2
		
		for y in range(window_size, height - window_size):
			for x in range(window_size, width - window_size):
				eigenx = derivative_x[y - window_size : y + window_size + 1, x - window_size : x + window_size + 1].sum()
				eigeny = derivative_y[y - window_size : y + window_size + 1, x - window_size : x + window_size + 1].sum()
				eigenxy = derivative_xy[y - window_size : y + window_size + 1, x - window_size : x + window_size + 1].sum()
				determinant = (eigenx * eigeny) - (eigenxy**2)
				trace = eigenx + eigeny
				corner_threshold = determinant - k*(trace**2)
				if corner_threshold > threshold_value:
					cv2.rectangle(new_temp_img,(x-5,y-5),(x+5,y+5),(0,0,255),0)
	
		return new_temp_img
if len(sys.argv) == 3:
	filename = sys.argv[1]
	filename1 = sys.argv[2]
	img = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
	img2 = cv2.imread(filename1,cv2.IMREAD_GRAYSCALE)
	win = 'corner detection1'
	cv2.imshow(win,img)
	win2 = 'corner detection2'
	cv2.imshow(win2,img2)
	cv2.createTrackbar('neighbourhoodsize',win,6,100,none)
	initial_trackbarpos_neighbourhoodsize = cv2.getTrackbarPos('neighbourhoodsize',win)
	cv2.createTrackbar('k',win,4,50,none)
	initial_trackbarpos_k = cv2.getTrackbarPos('k',win)
	cv2.createTrackbar('threshold_value',win,10000,90000,none)
	initial_trackbarpos_thresholdvalue = cv2.getTrackbarPos('threshold_value',win)
	gg = float(initial_trackbarpos_k * 0.01)
	cv2.createTrackbar('gaussian',win,1,10,none)
	final_img = harris_corner_detection(img,int(initial_trackbarpos_neighbourhoodsize),float(gg),int(initial_trackbarpos_thresholdvalue))
	cv2.imshow('harris_corner_detection',final_img)
	
	#second img 
	cv2.createTrackbar('neighbourhoodsize',win2,6,100,none)
	initial_trackbarpos_neighbourhoodsize2 = cv2.getTrackbarPos('neighbourhoodsize',win2)
	cv2.createTrackbar('k',win2,4,50,none)
	initial_trackbarpos_k2 = cv2.getTrackbarPos('k',win2)
	cv2.createTrackbar('threshold_value',win2,10000,90000,none)
	initial_trackbarpos_thresholdvalue2 = cv2.getTrackbarPos('threshold_value',win2)
	gg = float(initial_trackbarpos_k * 0.01)
	cv2.createTrackbar('gaussian',win2,1,10,none)
	final_img = harris_corner_detection(img2,int(initial_trackbarpos_neighbourhoodsize2),float(gg),int(initial_trackbarpos_thresholdvalue2))
	cv2.imshow('harris_corner_detection2',final_img)
	cv2.waitKey(0)
	
	#feature points matching
	image1 = img.copy()
	image2 = img2.copy()
	imgg = cv2.cvtColor(image1, cv2.COLOR_GRAY2RGB)
	imggg = cv2.cvtColor(image2, cv2.COLOR_GRAY2RGB)
	image3 = imgg.copy()
	image4 = imggg.copy()
	orb = cv2.ORB_create()
	kp1, des1 = orb.detectAndCompute(image1,None)
	kp2, des2 = orb.detectAndCompute(image2,None)
	bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	matches = bf.match(des1,des2)
	matches = sorted(matches, key = lambda x:x.distance)
	img3 = cv2.drawMatches(image1,kp1,image2,kp2,matches[:30],None, flags=2)
	cv2.imshow('feature matching',img3)
	list_kp1 = [kp1[mat.queryIdx].pt for mat in matches]
	list_kp2 = [kp2[mat.trainIdx].pt for mat in matches]
	for ii in range(0,31):
		st = str(ii)
		cv2.putText(image3,st,(int(list_kp1[ii][0]),int(list_kp1[ii][1])),cv2.FONT_HERSHEY_SCRIPT_SIMPLEX   , 1,(0,0,255), 2)
		cv2.putText(image4,st,(int(list_kp2[ii][0]),int(list_kp2[ii][1])),cv2.FONT_HERSHEY_SCRIPT_SIMPLEX   , 1,(0,0,255), 2)
	cv2.imshow('miracle',image3)
	cv2.imshow('miracle2',image4)
	
	while (1):
		neighbourhood_size = cv2.getTrackbarPos('neighbourhoodsize',win)
		k = cv2.getTrackbarPos('k',win)
		kk = float(k * 0.01)
		threshold_value = cv2.getTrackbarPos('threshold_value',win)
		gaussian = cv2.getTrackbarPos('gaussian',win)
		print neighbourhood_size, kk, threshold_value
		if neighbourhood_size != initial_trackbarpos_neighbourhoodsize and neighbourhood_size != -1 or k != initial_trackbarpos_k or threshold_value != initial_trackbarpos_thresholdvalue :
			cv2.destroyWindow('harris_corner_detection')
			initial_trackbarpos_neighbourhoodsize = neighbourhood_size
			initial_trackbarpos_k = k
			initial_trackbarpos_thresholdvalue = threshold_value
			img = cv2.GaussianBlur(img,(gaussian,gaussian),0)
			final_img = harris_corner_detection(img,int(neighbourhood_size),float(kk),int(threshold_value))
			cv2.imshow('harris_corner_detection1',final_img)
			tt = cv2.waitKey(0)
			if tt == 27:
				break
			
		elif neighbourhood_size == -1 :
			neighbourhood_size = initial_trackbarpos_neighbourhoodsize
			
		elif neighbourhood_size == initial_trackbarpos_neighbourhoodsize and k == initial_trackbarpos_k and threshold_value == initial_trackbarpos_thresholdvalue :
			break
			
			
	cv2.waitKey(0)
	
	
	while (1):
		neighbourhood_size2 = cv2.getTrackbarPos('neighbourhoodsize',win2)
		k = cv2.getTrackbarPos('k',win2)
		kk = float(k * 0.01)
		threshold_value2 = cv2.getTrackbarPos('threshold_value',win2)
		gaussian = cv2.getTrackbarPos('gaussian',win2)
		print neighbourhood_size2, kk, threshold_value2
		if neighbourhood_size2 != initial_trackbarpos_neighbourhoodsize2 and neighbourhood_size2 != -1 or k != initial_trackbarpos_k2 or threshold_value2 != initial_trackbarpos_thresholdvalue2 :
			cv2.destroyWindow('harris_corner_detection2')
			initial_trackbarpos_neighbourhoodsize2 = neighbourhood_size2
			initial_trackbarpos_k2 = k
			initial_trackbarpos_thresholdvalue2 = threshold_value2
			img2 = cv2.GaussianBlur(img2,(gaussian,gaussian),0)
			final_img2 = harris_corner_detection(img2,int(neighbourhood_size2),float(kk),int(threshold_value2))
			cv2.imshow('harris_corner_detection2',final_img2)
			tt = cv2.waitKey(0)
			if tt == 27:
				break
				
		elif neighbourhood_size2 == -1 :
			neighbourhood_size2 = initial_trackbarpos_neighbourhoodsize2
			
		elif neighbourhood_size2 == initial_trackbarpos_neighbourhoodsize2 and k == initial_trackbarpos_k2 and threshold_value2 == initial_trackbarpos_thresholdvalue2 :
			break

elif len(sys.argv) < 3:
	print "HARRIS CORNER DETECTION"
	print "*****======================================================================*****"
	print "Steps to run the program"
	print "to start the program use the following format"
	print "                            "
	print "                            "
	print " $python A.py file1.png file2.png"
	print "                            " 
	print "                            "
	print "here A.py is python code ,  file1.png and file2.png are two similar images"
	print "after running the command you will be prompted with 4 windows"
	print "two windows have trackbars , other two windows are outputs of corner detection"
	print "first modify the trackbar of harris_corner_dectection1 and press ESC key"
	print "This will generated the new image based on trackbar changes"
	print "similarly do the same for harris_corner_detection2 , change the trackbar and hit ESC"
	print "now enter ESC to obtain the feature matchings"
	cv2.destroyAllWindows()
	
	