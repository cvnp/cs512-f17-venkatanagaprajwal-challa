import tensorflow as tf
from model import DTN
from solver import Solver

flags = tf.app.flags
flags.DEFINE_string('mode', 'train', "'pretrain','help', 'train' or 'eval'")
flags.DEFINE_string('model_save_path', 'model', "directory for saving the model")
flags.DEFINE_string('sample_save_path', 'sample', "directory for saving the sampled images")
FLAGS = flags.FLAGS

def main(_):
    
    model = DTN(mode=FLAGS.mode, learning_rate=0.0003)
    solver = Solver(model, batch_size=100, pretrain_iter=20000, train_iter=2000, sample_iter=100, 
                    svhn_dir='svhn', mnist_dir='mnist', model_save_path=FLAGS.model_save_path, sample_save_path=FLAGS.sample_save_path)
    
    
    if not tf.gfile.Exists(FLAGS.model_save_path):
        tf.gfile.MakeDirs(FLAGS.model_save_path)
    if not tf.gfile.Exists(FLAGS.sample_save_path):
        tf.gfile.MakeDirs(FLAGS.sample_save_path)
    
	if FLAGS.mode == 'help':
	print("Welcome")	
	print("Following are the instructions to run the program")
	print("Required softwares : ")
	print("Tensorflow 0.12 +  pickle + Scipy")
	print("First create source and target directories names svhn as source directory and mnist as target directory respectively")
	print("in the svhn directory download the svhn data set from http://ufldl.stanford.edu/housenumbers , extract them  and save train data as train_32x32.mat , test data as test_32x32.mat , extra data as extra_32x32.mat in svhn directory")
	print("***********************************************************************************************************")
	print("***********************************************************************************************************") 
	print("  ")
	print("  ") 
	print("Now run the command : $python preprocessing.py")
	print("  ")
    print("  ") 
	print("***********************************************************************************************************")
    print("***********************************************************************************************************") 
	print("  ")
	print("  ")
	print("after preprocessing run : $python main.py --mode='pretrain'")
	print("  ")
    print("  ")
    print("***********************************************************************************************************")
    print("***********************************************************************************************************")
	print("after pretraining run : $python main.py --mode='train' ")
	print("  ")
    print("  ")
    print("***********************************************************************************************************")
    print("***********************************************************************************************************")
	print("after traning run : $python main.py --mode='eval' ")
	print("  ")
    print("  ")
    print("***********************************************************************************************************")
    print("***********************************************************************************************************")
	print("images generated will be saved in sample folder")
    elif FLAGS.mode == 'pretrain':
        solver.pretrain()
    elif FLAGS.mode == 'train':
        solver.train()
    else:
        solver.eval()
        
if __name__ == '__main__':
    tf.app.run()